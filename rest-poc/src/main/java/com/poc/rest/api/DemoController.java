package com.poc.rest.api;

import com.poc.rest.api.dto.CustomerDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Api("POC API.")
@RequestMapping("")
@RequiredArgsConstructor
public class DemoController {

    @GetMapping("/v1/customers/{id}")
    @ApiModelProperty("Get salary card order list of a customer V1.")
    public CustomerDTO getOrdersV1(@PathVariable int id){
          return CustomerDTO.builder().id(id).name(id+"-nameV1").build();
    }

    @GetMapping("/v2/customers/{id}")
    @ApiModelProperty("Get salary card order list of a customer V2.")
    public CustomerDTO getOrdersV2(@PathVariable int id){
        return CustomerDTO.builder().id(id).name(id+"-nameV2").build();
    }
}
